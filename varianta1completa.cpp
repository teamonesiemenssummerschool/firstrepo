#include <stdio.h>
#include <iostream>
#include <stdlib.h> 
#include <time.h> 
#include <stdlib.h>

const int default_lower_range = 1;
const int default_higher_range = 100;

void userGuess(int secretNumber)
{
	int input;
	int numberofGuesses = 0;

	std::cout << "guess the number:";
	while (true)
	{
		numberofGuesses++;
		std::cin >> input;
		if (input == secretNumber)
		{
			std::cout << "guessed correctly"<<std::endl;
			break;
		}
		else
			if (input > secretNumber)
				std::cout << "too high" << std::endl;
			else
				std::cout << "too low" << std::endl;

	}
	std::cout << numberofGuesses;
}

void pcGuess(int lowerRange, int higherRange)
{
	bool answer;
	int guess = (higherRange + lowerRange) / 2;
	int higherOrLower;

	while (true)
	{
		std::cout << "is this the number : " << guess << std::endl;
		std::cin >> answer;
		if (answer == true)
		{
		std::cout << "I did it :)";
		return;
	}
		else
		{
			std::cout << "enter 0 if value is lower and 1 if higher:" << std::endl;
			std::cin >> higherOrLower;
			switch (higherOrLower)
			{
			case 1:
				pcGuess(guess, higherRange);
				break;
			case 0:
				pcGuess(lowerRange, guess);
				break;
			}
		}
	}
}

void get_prog_argv(
	int argc,
	const char** argv,
	int& lower_range,
	int& higher_range,
	int& seed)
{
	if (argc > 1)
		lower_range = atoi(argv[1]);
	else
		lower_range = default_lower_range;

	if (argc > 2)
		higher_range = atoi(argv[2]);
	else
		higher_range = default_higher_range;	

	if (argc > 3)
		seed = atoi(argv[3]);
	else
		seed = time(NULL);
}

int main(int argc, const char** argv) 
{
	int lower_range, higher_range, seed;

	get_prog_argv(argc, argv, lower_range, higher_range, seed);
	srand(seed);

	int secretNumber = rand() % higher_range + lower_range;
	
	userGuess(secretNumber);
	// pcGuess(lower_range, higher_range);
	return 0;

}
